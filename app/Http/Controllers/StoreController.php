<?php

namespace App\Http\Controllers;

// use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Contracts\Cache\Store;
use Illuminate\Cache\CacheManager;
use Illuminate\Support\Facades\Redis;
// use Cache;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        //
    }

    public function getValues($keys=null){
        $redis= Redis::connection();
        $ttl =300;
        // Redis::flushDB();
        if ($keys == null) {    
            $allKeys = $redis->keys('*');
            foreach ($allKeys as $key){
                $value1=  $redis->get($key);
                $items[] = [$key => $value1];
            }

        }else{  
                $array_key = explode(',',$keys);
                foreach ($array_key as $key){
                $value1=  $redis->get($key);
                $redis->expire($keys,$ttl); 
                $items[] = [$key => $value1];
                }
            }
        return response()->json($items, 200);
    }

      /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeValues(Request $request){
        $data = $request->all();
        $ttl =300;
        $redis = Redis::connection();
        foreach($data as $key => $value) {
            $keys = str_replace( ',', '', $key );
            $values = str_replace( ',', '', $value );
            $redis->set($keys, $values);
            $redis->expire($keys,$ttl); 
        }
        return response()->json(['Message' =>'Key Value has been successfully stored' ], 200);
    }
      /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function editStoreValues(){
        $data = request()->all();
        $redis = Redis::connection();
        $ttl =300;
        foreach($data as $key => $value) {
            $keys = str_replace( ',', '', $key );
            $values = str_replace( ',', '', $value );
            $redis->set($keys, $values);
            $redis->expire($keys,$ttl); 
        }
        return response()->json(['Message' =>'Value of key has been successfully updated' ], 200);
      
    }

    
}
